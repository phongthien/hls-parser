package main

import (
	"bytes"
	m3u8 "gitlab.com/phongthien/hls-parser"
	"log"
)

//
//import (
//	"bytes"
//	"github.com/nguyentruongiang/hls-parser"
//	"log"
//)
//
const data = `#EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:2
#EXT-X-KEY:METHOD=AES-128,URI="http://124.30.172.11:2260/key/key.bin"
#EXT-X-MEDIA-SEQUENCE:1605631
#EXTINF:2.000000,
#EXT-X-CUE-OUT-CONT:ElapsedTime=7,Duration=15,SCTE35=/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
360-1605631.ts
#EXTINF:2.000000,
#EXT-X-CUE-OUT-CONT:ElapsedTime=9,Duration=15,SCTE35=/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
360-1605632.ts
#EXTINF:2.000000,
#EXT-X-CUE-OUT-CONT:ElapsedTime=11,Duration=15,SCTE35=/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
360-1605633.ts
#EXTINF:2.000000,
#EXT-X-CUE-OUT-CONT:ElapsedTime=13,Duration=15,SCTE35=/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
360-1605634.ts
#EXTINF:2.000000,
#EXT-X-CUE-OUT-CONT:ElapsedTime=15,Duration=15,SCTE35=/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
360-1605635.ts
#EXTINF:2.000000,
#EXT-OATCLS-SCTE35:/DAgAAAAAAAAAP/wDwUAAYyWf//+ABSZcAAAAAAAACQCmjY=
#EXT-X-CUE-IN
360-1605636.ts
#EXTINF:2.000000,
360-1605637.ts
#EXTINF:2.000000,
360-1605638.ts
#EXTINF:2.000000,
360-1605639.ts
#EXTINF:2.000000,
#EXT-OATCLS-SCTE35:/DAgAAAAAAAAAP/wDwUAAYyef//+ABSZcAAAAAAAABI+11A=
#EXT-X-CUE-OUT:15
360-1605640.ts
`

func main() {

	a, _ := m3u8.NewMediaPlaylist(1, 10)
	ex := bytes.NewBufferString(data)
	a.Decode(*ex, true)

	log.Print(a.Segments[0])
	for i := 0; i < len(a.Segments); i++ {
		log.Println(a.Segments[i].SeqId)
	}
	a.Segments[0].Key = nil
	cv, _ := m3u8.NewMediaPlaylist(0, 0)
	cv.SeqNo = a.SeqNo
	cv.TargetDuration = a.TargetDuration
	cv.Segments = a.Segments
	cv.Key = a.Key
	cv.SetWinSize(uint(len(a.Segments)))
	cv.SetCount(uint(len(a.Segments)))
	//for i := 0; i < len(a.Segments); i++ {
	//	cv.AppendSegment(a.Segments[i])
	//}
	log.Print(cv.Encode().String() == data)
	log.Print(cv.Encode().String())
}
